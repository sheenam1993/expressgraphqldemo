const FormattableError = require('./formattable-error');
// import FormattableError from './formattable-error' ;
const {NotFound} = require('./known-error')
// import {NotFound} from './known-error';
 const createError = ({name, statusCode}) =>{
    if (!name) {
      throw new Error('InsufficientArguments: "name" is required to instantiate FormattableError');
    }
    if (!statusCode) {
      throw new Error('InsufficientArguments: "statusCode" is required to instantiate FormattableError');
    }
    return class extends FormattableError {
  constructor({ message, data }) {
       console.log(message, 'message ==> inside create error');
    super({
      name,
      statusCode,
      message,
      data
    });
  }
}
} ; 
 const format = (err) => {
    // console.log(FormattableError,"formattable error==>> inside format")
  const { message } = err;
//    console.log('error ==>>>', err);
    //  console.log('check instance ==>>', err.originalError instanceof FormattableError);
  if (err.originalError instanceof FormattableError) {
   return err.originalError.serialize() ;
  }

  return {
    message: err.message,
    name: 'UnknownError',
    statusCode: 500,
    data: {}
  };
}


module.exports = {
  createError : createError,
  format : format
}

