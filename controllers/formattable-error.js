 const { ExtendableError } = require('extendable-error');
// import ExtendableError from 'extendable-error' ;
console.log(ExtendableError,"extendable error ==>>")
 class FormattableError extends ExtendableError {
  constructor({
    name, statusCode, message, data
  }) {
    if (!message) {
      throw new Error('InsufficientArguments: "message" is required to instantiate FormattableError');
    }
    if (!name) {
      throw new Error('InsufficientArguments: "name" is required to instantiate FormattableError');
    }
    if (!statusCode) {
      throw new Error('InsufficientArguments: "statusCode" is required to instantiate FormattableError');
    }
    console.log("inside formattable error ==>",name,statusCode,message,data)
    super(message);
    this.name = name;
    this.message = message;
    this.statusCode = statusCode;
    this.data = data;

  }

  serialize() {
    const {
      name, statusCode, message, data
    } = this;
    console.log("serialize called ==>>")
    return {
      name,
      statusCode,
      message,
      data
    };
  }
}
 module.exports = FormattableError;
