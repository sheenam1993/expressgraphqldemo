 import {createError} from './create-error' ;

// const a = require('./create-error') ;

// describe all types of errors here
const NotFound = createError({name : 'NotFound',statusCode : 400}) ;

// call this function in catch block to re-throw error in requied format
const catchFunction = (error)=>{
    console.log("inside catch function ==>>",error)
    if(error.name == 'NotFound'){
        throw new NotFound({message : error.message,data : error.data}) ;
    }
};
 module.exports = {NotFound , catchFunction};
