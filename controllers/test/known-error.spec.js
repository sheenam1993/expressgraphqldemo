const { expect } = require('chai');
const chance = require('chance').Chance();
const FormattableError = require('../formattable-error');
const {catchFunction , NotFound} = require('../known-error');
const {createError} = require('../create-error')
console.log(catchFunction,"catch function ==>")
describe('Lib: known errors - catch function  ', () => {
  it('should throw error according to name provided', () => {
      let error = {
          name : 'NotFound',
          statusCode : 400 ,
          message : 'user not found',
          data : {}
      }
    expect(()=>catchFunction(error)).to.throw(new NotFound({message : error.message , data : error.data}))


  });
 
});

