const { expect } = require('chai');
const chance = require('chance').Chance();
const FormattableError = require('../formattable-error');
describe('Lib: FormattableError', () => {
  it('should throw if required attributes are not provided: name, statusCode, message', () => {
    expect(() => new FormattableError({
      name: chance.string(),
      statusCode: chance.integer()
    })).to.throw('InsufficientArguments: "message" is required to instantiate FormattableError');

    expect(() => new FormattableError({
      message: chance.string(),
      statusCode: chance.integer()
    })).to.throw('InsufficientArguments: "name" is required to instantiate FormattableError');

    expect(() => new FormattableError({
      name: chance.string(),
      message: chance.integer()
    })).to.throw('InsufficientArguments: "statusCode" is required to instantiate FormattableError');
  });

  it('should have required properties: name, message, statusCode, data', () => {
    const payload = {
      name: chance.string({ length: 5 }),
      statusCode: chance.integer(),
      message: chance.string({ length: 10 }),
      data: {}
    };
    const err = new FormattableError(payload);
    expect(err).to.contain.keys('data', 'statusCode', 'name');
  });

  describe('.serialize', () => {
    it('should convert this to an object', () => {
      const payload = {
        name: chance.string({ length: 5 }),
        statusCode: chance.integer(),
        message: chance.string({ length: 10 }),
        data: {}
      };
      const err = new FormattableError(payload);
      expect(err.serialize()).to.contain.keys('data', 'statusCode', 'name', 'message');
    });
  });
});
