const { expect } = require('chai');
const chance = require('chance').Chance();
const FormattableError = require('../formattable-error');
const { format, createError } = require('../create-error');
console.log(format, "formatt ==>>")
describe('Lib: create error - format function  ', () => {
  it('should return named error if instance of formattable error is passed', () => {
    let err = {
      originalError: new FormattableError({
        name: chance.string({ length: 5 }),
        statusCode: chance.integer(),
        message: chance.string({ length: 10 }),
        data: {}
      })
    }
    let formatedError = format(err);
    expect(formatedError).to.contain.keys('data', 'statusCode', 'name', 'message');
    expect(formatedError.name).to.be.equal(err.originalError.name);
    expect(formatedError.statusCode).to.be.equal(err.originalError.statusCode);
    expect(formatedError.message).to.be.equal(err.originalError.message);
    expect(formatedError.data).to.eql(err.originalError.data);


  });
  it('should return unknown error if random error occurs', () => {
    let error = new Error('hello , i am not an instance of formattable error');
    let formatedError = format(error);
    console.log(formatedError, "formated error ==>")
    expect(formatedError).to.contain.keys('data', 'statusCode', 'name', 'message');
    expect(formatedError.name).to.be.equal('UnknownError');
    expect(formatedError.statusCode).to.be.equal(500);
    expect(formatedError.message).to.be.equal(error.message);
    expect(formatedError.data).to.eql({});

  });
});

describe('Lib: create error - createError function  ', () => {
  it('should throw if required attributes are not provided: name, statusCode', () => {
    expect(() => createError({ statusCode: 400 })).to.throw('InsufficientArguments: "name" is required to instantiate FormattableError');
  });
  it('should throw if required attributes are not provided: name, statusCode', () => {
    expect(() => createError({ name: 'not found' })).to.throw('InsufficientArguments: "statusCode" is required to instantiate FormattableError');
  });
  it('should return an anonymous class which is instance of Formattable error', () => {
    let anonymousClass = createError({name : 'AlreadyRegistered',statusCode : 400});
    let classInstance = new anonymousClass({ message: "hi", data: {} })
    let checkInstance = classInstance instanceof FormattableError;
    console.log(checkInstance, "check instance =>>")
    expect(checkInstance).to.be.true;
  })
});
