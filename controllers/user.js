
/*
 * @file: user.js
 * @description: Controllers regarding user services.
 * @date: 20.7.2018
 * @author:sheenam
 * */

 import {addUserService} from '../services/user';
// import {UnknownError,CustomSuccessResponse,AlreadyRegistered} from '../utilities/CreateErrors' ;
import {NotFound , catchFunction} from './known-error' ;
import FormattableError from './formattable-error' ;

export const addUser = async (root, payload, context) => {
    try{
    let result = await addUserService(payload) ;

    if(result.emailAlreadyexists){
        console.log("inside not found blockk=>>>")
        throw new NotFound({message : "not found sheenam" ,  data : {email : "abc"}}) ;
    }
    if(result){
        return result
    }
    }catch(error){
        // call function to check appropriate error
        if(error instanceof FormattableError){
            console.log("going to call catch function==>>>")
             catchFunction(error);
        }else{
            throw new Error(error);
        }
     }
}